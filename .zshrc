#
# User configuration sourced by interactive shells
#

# Change default zim location
export ZIM_HOME=${ZDOTDIR:-${HOME}}/.zim

# Start zim
[[ -s ${ZIM_HOME}/init.zsh ]] && source ${ZIM_HOME}/init.zsh

# The following lines were added by compinstall
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' matcher-list '' 'l:|=* r:|=*' 'r:|[._-]=* r:|=*' 'm:{[:lower:]}={[:upper:]} m:{[:lower:][:upper:]}={[:upper:][:lower:]}'
zstyle :compinstall filename '/home/yack/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install

# Personal stuff is personal stuff
alias ls='ls --color=auto'
alias ana='source /home/yack/doc/ana/bin/activate'
alias dana='source /home/yack/doc/ana/bin/deactivate'
alias dots='git --git-dir=$HOME/.dots.git/ --work-tree=$HOME'
alias dan='bash -e "source ~/doc/venv/bin/activate & source build/envsetup.sh"'
export PATH="/home/yack/bin:/home/yack/.gem/ruby/2.5.0/bin/:$PATH"
